package pages;

import models.Navigation;
import org.openqa.selenium.By;
import utilities.Driver;

public class BasePage {

    private static By acceptCookie  = By.id("Row1_Column1_Cell1_CookieSettings_AdvancedSaveAccept");

    public static String GoTo(String page) {

        switch (page) {
            case "homePage":
                return Navigation.getHomePage();
            case "loginPage":
                return Navigation.getLoginPage();
            default:
                return (page + " not declared");
        }
    }

    public static void AcceptCookies() {

        Driver.Instance.findElement(acceptCookie).click();

    }

    public static String getUrl() {
        return Driver.Instance.getCurrentUrl();
    }

}

