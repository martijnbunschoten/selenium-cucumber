package pages;

import models.Users;
import org.openqa.selenium.By;
import utilities.Driver;

import java.sql.SQLException;

public class LoginPage extends BasePage {

   private By usernameInput = By.id("Row1_Column1_Cell2_Login_Username");
   private By passwordInput = By.id("Row1_Column1_Cell2_Login_Password");
   private By loginButton   = By.id("Row1_Column1_Cell2_Login_LoginButton");


    public static void navigateTo(String page) {
        Driver.Instance.get(GoTo(page));
    }


    public LoginPage enterFalsePassword(String falsePassword) {
        Driver.Instance.findElement(passwordInput).sendKeys(falsePassword);
        return this;
    }

    public LoginPage enterUsername(String username) throws SQLException {
        Driver.Instance.findElement(usernameInput).sendKeys(Users.GetUsername());
        return this;

    }

   public LoginPage enterPassword(String username) throws SQLException {
    Driver.Instance.findElement(passwordInput).sendKeys(Users.GetPassword());
    return this;
   }

   public OverzichtPage clickLoginButton() {
       Driver.Instance.findElement(loginButton).click();
       return new OverzichtPage();
   }

    public OverzichtPage Login(String username, String password) {
        Driver.Instance.findElement(usernameInput).sendKeys(username);
        Driver.Instance.findElement(passwordInput).sendKeys(password);
        // click login button
        return new OverzichtPage();
    }


}




