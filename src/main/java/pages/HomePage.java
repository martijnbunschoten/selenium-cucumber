package pages;

import org.openqa.selenium.By;
import utilities.Driver;

public class HomePage extends BasePage {

private static By myT_mobileButton = By.xpath("//a[@class='navbar-link navbar-user-toggle']");


    public static void navigateTo(String page) {
        Driver.Instance.get(GoTo(page));
    }


    public static LoginPage clickMyTmobileButton() {
        Driver.Instance.findElement(myT_mobileButton).click();
        return new LoginPage();
    }





}
