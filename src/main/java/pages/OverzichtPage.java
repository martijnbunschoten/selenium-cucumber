package pages;


import org.openqa.selenium.By;
import utilities.Driver;

public class OverzichtPage extends BasePage {

    private static By ChangeAbboButton = By.xpath("//a[@class='button-call-to-action button-mobile-block']");


    public BundelsAanpassenPage clickChangeAbboButton() {
        Driver.Instance.findElement(ChangeAbboButton).click();
        return new BundelsAanpassenPage();
    }
}


