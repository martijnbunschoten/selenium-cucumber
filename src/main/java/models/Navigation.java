package models;

public class Navigation {

    private static String homePage = "https://www.t-mobile.nl";
    private static String loginPage = "https://www.t-mobile.nl/login";

    public static String getHomePage() {
        return homePage;
    }

    public static String getLoginPage() {
        return loginPage;
    }
}
