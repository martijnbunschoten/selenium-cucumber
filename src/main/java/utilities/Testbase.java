package utilities;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.sql.SQLException;

import static pages.BasePage.GoTo;

public class Testbase extends SQLconnection {

    @BeforeClass
    public static void Setup() throws SQLException {
        // initializing driver
        Driver.Initialize();

        // initializing sql connection
        SQLconnection.getDataBaseConnection();
    }


    // closes current browser session
    @AfterClass
    public static void Cleanup() {
        Driver.Close();
    }

    public static void navigateTo(String page) {
        Driver.Instance.get(GoTo(page));
    }

    } // end method
