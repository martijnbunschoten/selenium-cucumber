package seleniumtests;

import models.Users;
import org.junit.Assert;
import org.junit.Test;
import pages.OverzichtPage;
import pages.HomePage;
import utilities.Testbase;

import java.sql.SQLException;

public class LoginTest extends Testbase {

    @Test
    public void succesfullLogin() throws SQLException {
    navigateTo("homePage");
    HomePage.AcceptCookies(); //implement return current page to prevent repeating classname in the upcoming steps
    HomePage.clickMyTmobileButton()
    .enterUsername(Users.GetUsername())
    .enterPassword(Users.GetPassword())
    .clickLoginButton();

    Assert.assertEquals(OverzichtPage.getUrl(), "https://www.t-mobile.nl/my/dashboard");
    }

    /*
   @Test
    public void loginErrorOnWrongPassword() throws SQLException {
       navigateTo("homePage");
       HomePage.AcceptCookies();
       HomePage.clickMyTmobileButton()
       .enterUsername(Users.GetUsername())
       .enterFalsePassword(Users.getFalsePassword())
       .clickLoginButton();

       Assert.assertEquals(OverzichtPage.getUrl(), "https://www.t-mobile.nl/login");
       Assert.assertEquals(LoginPage.getErrorMessage(), "Je gebruikersnaam en/of het wachtwoord is onjuist. Probeer het opnieuw.");
   }
   */
}
