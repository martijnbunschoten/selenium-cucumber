package cucumbertest.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.OverzichtPage;
import pages.LoginPage;

public class StepDefinitions {


    @Given("^: I am on the loginpage of T-mobile$")
    public static void iAmOnTheLoginPage()  {

        LoginPage.navigateTo("loginPage");
        LoginPage.AcceptCookies();

    }

    @When("^: I enter my credentials and click the login button$")
    public static void iEnterCredentialsAndClickLogin() {
        // LoginPage.LoginAs("martijn.bunschoten@student.hu.nl").WithPassword("Breakdance9119$").Login();


    }


    @Then("^: I should see my personal page$")
    public static void iamOnMyPersonalPage()  {
        Assert.assertEquals(OverzichtPage.getUrl(), "https://www.t-mobile.nl/my/dashboard");

    }

}


