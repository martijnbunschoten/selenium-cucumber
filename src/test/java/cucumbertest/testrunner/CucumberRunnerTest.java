package cucumbertest.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import utilities.Testbase;


@RunWith(Cucumber.class)
@CucumberOptions(  monochrome = true,
        features = "src/test/resources/features",
        glue = "cucumbertest/steps" )

public class CucumberRunnerTest extends Testbase {

}



